import React from 'react';
import { RootNavigationContainer } from './navigation';
import { ThemeProvider } from './components';

const App = () => {
	return (
		<ThemeProvider>
			<RootNavigationContainer />
		</ThemeProvider>
	);
};

export default App;