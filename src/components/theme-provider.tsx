import React, { createContext, useContext } from 'react';

export const colors = {
    white: '#FFFFFF',
    black: '#000000',
    accentNest: '#00DCF0',
    caution200: '#FCF0CD',
    caution300: '#FEEA8A',
    caution400: '#EDC200',
    caution500: '#9C6F19',
    destructive200: '#FBEAE5',
    destructive300: '#FEAE9A',
    destructive400: '#DF3617',
    destructive500: '#BF0A12',
    primary100: '#F7FBFF',
    primary200: '#E6EBF2',
    primary300: '#CFD7E5',
    primary400: '#8F9CB2',
    primary500: '#172B4D',
    success200: '#E3F1DE',
    success300: '#BAE5B3',
    success400: '#4FB83D',
    success500: '#0F8043',
};

export const DEFAULT = {
    baseSpace: 5,
    colors,
};

export const ThemeContext = createContext(DEFAULT);
export const ThemeProvider: React.FC<any> = ({ children }) => {
    return (
        <ThemeContext.Provider value={DEFAULT}>
            {children}
        </ThemeContext.Provider>
    );
};
export const useTheme = () => useContext(ThemeContext);